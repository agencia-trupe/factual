<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfeccaoTable extends Migration
{
    public function up()
    {
        Schema::create('confeccao', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('confeccao');
    }
}
