<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniformesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uniformes_categorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('uniformes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uniformes_categoria_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('nome');
            $table->string('slug');
            $table->string('capa');
            $table->string('imagem');
            $table->text('descricao');
            $table->timestamps();
            $table->foreign('uniformes_categoria_id')->references('id')->on('uniformes_categorias')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('uniformes');
        Schema::drop('uniformes_categorias');
    }
}
