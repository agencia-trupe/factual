<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email' => 'factual@factualuniformes.com.br',
            'telefone' => '11 2636·4032',
            'endereco' => '<p>Rua do Endereço, 123</p><p>01234-567 - São Paulo, SP',
            'google_maps' => '',
            'facebook' => '/',
        ]);
    }
}
