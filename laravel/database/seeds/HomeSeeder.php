<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'chamada_1' => '<h2>Tecidos <br>diferenciados</h2><p>Utilizamos matéria prima da mais alta qualidade, desenvolvemos sempre produtos exclusivos, proporcionando identidade e fixação da marca dos nossos clientes.</p>',
            'chamada_2' => '<h2>Fabricação <br>própria</h2><p>A Factual conta com equipe especializada. Toda sua produção, do corte ao acabamento, é feita na própria fábrica, resultando em melhor qualidade e agilidade em sua produção.</p>',
            'chamada_3' => '<h2>Equipamentos <br>de ponta</h2><p>Nossos equipamentos são novos e modernos, proporcionando qualidade e agilidade na confecção de nossos produtos.</p>',
            'texto' => '<p>A <strong>Factual</strong> tem como objetivo melhorar seus profissionais e a qualidade de seus produtos. Fundada em 1996 especializou-se no segmento de uniformes escolares, sempre trabalhando com dedicação e carinho. Nosso foco é oferecer o melhor atendimento proporcionando conforto e satisfação dos nossos clientes.</p>',
        ]);
    }
}
