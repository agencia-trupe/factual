@extends('frontend.common.template')

@section('content')

    <section class="content empresa">
        <div class="content-title">
            <div class="center">
                <h1>Empresa</h1>
            </div>
        </div>

        <div class="content-body">
            <div class="center">
                <div class="empresa-texto">
                    {!! $empresa->texto_1 !!}
                </div>

                <div class="empresa-texto">
                    {!! $empresa->texto_2 !!}
                </div>
            </div>
        </div>

        <div class="empresa-categorias">
            <div class="center">
                @foreach($categorias as $categoria)
                <a href="{{ route('uniformes', $categoria->slug) }}">{{ $categoria->titulo }}</a>
                @endforeach
            </div>
        </div>
    </section>

@endsection
