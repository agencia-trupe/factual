@extends('frontend.common.template')

@section('content')

    <section class="content confeccao">
        <div class="content-title">
            <div class="center">
                <h1>Confecção</h1>
            </div>
        </div>

        <div class="content-body">
            <div class="center">
                <div class="texto">
                    {!! $confeccao->texto !!}
                </div>

                @if(count($imagens))
                <div class="imagens">
                    @foreach($imagens as $imagem)
                    <a href="{{ asset('assets/img/confeccao/'.$imagem->imagem) }}" rel="confeccao" class="fancybox imagem" title="{{ $imagem->legenda }}">
                        <img src="{{ asset('assets/img/confeccao/thumbs/'.$imagem->imagem) }}" alt="">
                        @if($imagem->legenda) <span>{{ $imagem->legenda }}</span> @endif
                    </a>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </section>

@endsection
