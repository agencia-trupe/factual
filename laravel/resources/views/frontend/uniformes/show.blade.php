@extends('frontend.common.template')

@section('content')

    <section class="content uniformes">
        <div class="content-title">
            <div class="center">
                <h1>Uniformes Escolares</h1>
            </div>
        </div>

        <div class="content-body">
            <div class="center">
                @include('frontend.uniformes._categorias')

                <div class="uniformes-detalhe">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/uniformes/'.$uniforme->imagem) }}" alt="">
                    </div>

                    <div class="info">
                        <h1>{{ $uniforme->nome }}</h1>
                        <div class="descricao">
                            {!! $uniforme->descricao !!}
                        </div>

                        <form action="" method="POST" id="form-contato">
                            <h3>Mais informações e pedidos:</h3>
                            <input type="text" name="nome" id="nome" placeholder="nome" required>
                            <input type="email" name="email" id="email" placeholder="e-mail" required>
                            <input type="text" name="telefone" id="telefone" placeholder="telefone">
                            <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                            <input type="submit" value="ENVIAR">
                            <div id="form-contato-response"></div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection
