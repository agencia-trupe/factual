<div class="uniformes-categorias">
    <h3>Nossos Produtos</h3>

    @foreach($categorias as $cat)
    <a href="{{ route('uniformes', $cat->slug) }}" @if($cat->slug === $categoria->slug) class="active" @endif>
        {{ $cat->titulo }}
    </a>
    @endforeach
</div>