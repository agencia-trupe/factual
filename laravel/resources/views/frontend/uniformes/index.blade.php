@extends('frontend.common.template')

@section('content')

    <section class="content uniformes">
        <div class="content-title">
            <div class="center">
                <h1>Uniformes Escolares</h1>
            </div>
        </div>

        <div class="content-body">
            <div class="center">
                @include('frontend.uniformes._categorias')

                <div class="uniformes-lista">
                @foreach($uniformes as $uniforme)
                    <a href="{{ route('uniformes.show', [$uniforme->categoria->slug, $uniforme->slug]) }}" class="uniforme-thumb">
                        <img src="{{ asset('assets/img/uniformes/thumbs/'.$uniforme->capa) }}" alt="">
                        <span>{{ $uniforme->nome }}</span>
                    </a>
                @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection
