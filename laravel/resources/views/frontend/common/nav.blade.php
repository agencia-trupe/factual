<a href="{{ route('home') }}"
    @if(str_is('home', Route::currentRouteName())) class="active" @endif>
    Home
</a>
<a href="{{ route('empresa') }}"
    @if(str_is('empresa', Route::currentRouteName())) class="active" @endif>
    Empresa
</a>
<a href="{{ route('uniformes') }}"
    @if(str_is('uniformes*', Route::currentRouteName())) class="active" @endif>
    Uniformes Escolares
</a>
<a href="{{ route('confeccao') }}"
    @if(str_is('confeccao', Route::currentRouteName())) class="active" @endif>
    Confecção
</a>
<a href="{{ route('clientes') }}"
    @if(str_is('clientes', Route::currentRouteName())) class="active" @endif>
    Clientes
</a>
<a href="{{ route('contato') }}"
    @if(str_is('contato', Route::currentRouteName())) class="active" @endif>
    Contato
</a>