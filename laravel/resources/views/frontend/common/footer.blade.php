    <section class="newsletter">
        <div class="center">
            <form action="" id="form-newsletter" method="POST">
                <p>Cadastre-se para acompanhar nossas novidades:</p>
                <input type="email" name="email" id="newsletter_email" placeholder="e-mail" required>
                <input type="submit" value="OK">
                <div class="response-wrapper">
                    <span id="form-newsletter-response"></span>
                </div>
            </form>

            <div class="newsletter-contato">
                <p>
                    {{ $contato->telefone }}
                    <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                </p>
                @if($contato->facebook)
                <a href="{{ $contato->facebook }}" class="facebook"></a>
                @endif
            </div>
        </div>
    </section>

    <footer>
        <div class="center">
            <div class="col">
                <a href="{{ route('empresa') }}">Empresa</a>
                <a href="{{ route('confeccao') }}">Confecção</a>
                <a href="{{ route('clientes') }}">Clientes</a>
            </div>

            <div class="col">
                <a href="{{ route('uniformes') }}">Uniformes Escolares</a>
                @foreach($uniformes_categorias as $cat)
                <a href="{{ route('uniformes', $cat->slug) }}" class="cat">{{ $cat->titulo }}</a>
                @endforeach
            </div>

            <div class="col contato">
                <a href="{{ route('contato') }}">Contato</a>
                <p><strong>{{ $contato->telefone }}</strong></p>
                <div class="endereco">{!! $contato->endereco !!}</div>
            </div>

            <div class="copyright">
                <p>
                    © {{ date('Y') }} {{ config('site.name') }}<br>
                    Todos os direitos reservados.
                </p>
                <p>
                    <a href="http://www.trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
