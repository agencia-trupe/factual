    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>
            <nav id="nav-desktop">
                @include('frontend.common.nav')
            </nav>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <nav id="nav-mobile">
            @include('frontend.common.nav')
        </nav>
    </header>
