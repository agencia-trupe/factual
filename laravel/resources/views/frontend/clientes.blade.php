@extends('frontend.common.template')

@section('content')

    <section class="content clientes">
        <div class="content-title">
            <div class="center">
                <h1>Clientes</h1>
            </div>
        </div>

        <div class="content-body">
            <div class="center">
                <div class="clientes-lista">
                @foreach($clientes as $cliente)
                    <div class="cliente">
                        <div class="imagem">
                            <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="">
                        </div>
                        <span>{{ $cliente->nome }}</span>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection
