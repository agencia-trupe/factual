@extends('frontend.common.template')

@section('content')

    <section class="home">
        <div class="banners">
            <div class="banners-cycle">
                @foreach($banners as $banner)
                <div class="slide">
                    <div class="center">
                        <div class="imagem">
                            <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="">
                        </div>

                        <div class="info">
                            <div class="titulo">{!! $banner->titulo !!}</div>
                            <p class="texto">{{ $banner->texto }}</p>
                            <a href="{{ $banner->link }}">{{ $banner->botao }}</a>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="cycle-pager"></div>
        </div>

        <div class="chamadas">
            <div class="center">
                <div class="chamada">
                    <img src="{{ asset('assets/img/layout/icone-tecidos.png') }}" alt="">
                    {!! $home->chamada_1 !!}
                </div>
                <div class="chamada">
                    <img src="{{ asset('assets/img/layout/icone-fabricacao.png') }}" alt="">
                    {!! $home->chamada_2 !!}
                </div>
                <div class="chamada">
                    <img src="{{ asset('assets/img/layout/icone-equipamentos.png') }}" alt="">
                    {!! $home->chamada_3 !!}
                </div>
            </div>
        </div>

        <div class="factual">
            <div class="center">
                {!! $home->texto !!}

                <img src="{{ asset('assets/img/layout/icone-factual-texto.png') }}" alt="">
            </div>
        </div>
    </section>

@endsection
