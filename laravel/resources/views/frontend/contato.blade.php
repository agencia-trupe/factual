@extends('frontend.common.template')

@section('content')

    <section class="content contato">
        <div class="content-title">
            <div class="center">
                <h1>Contato</h1>
            </div>
        </div>

        <div class="content-body">
            <div class="center">
                <div class="contato-info">
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <div class="endereco">{!! $contato->endereco !!}</div>
                    <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>

                    <form action="" method="POST" id="form-contato">
                        <h2>Fale conosco</h2>
                        <input type="text" name="nome" id="nome" placeholder="nome" required>
                        <input type="email" name="email" id="email" placeholder="e-mail" required>
                        <input type="text" name="telefone" id="telefone" placeholder="telefone">
                        <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                        <input type="submit" value="ENVIAR">
                        <div id="form-contato-response"></div>
                    </form>
                </div>
            </div>

            <div class="mapa">
                {!! $contato->google_maps !!}
            </div>
        </div>
    </section>

@endsection
