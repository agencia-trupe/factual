@include('<?=$namespace?>.common.flash')

<?php foreach($gen->fields as $field): ?>
<div class="form-group">
    {!! Form::label('<?=$field['name']?>', '<?=$field['alias']?>') !!}
    {!! Form::<?=($field['type'] == 'text' ? 'textarea' : 'text')?>('<?=$field['name']?>', null, ['class' => 'form-control']) !!}
</div>

<?php endforeach; ?>
{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
