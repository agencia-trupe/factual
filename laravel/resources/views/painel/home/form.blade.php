@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('chamada_1', 'Chamada 1') !!}
    {!! Form::textarea('chamada_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'chamada']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada_2', 'Chamada 2') !!}
    {!! Form::textarea('chamada_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'chamada']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada_3', 'Chamada 3') !!}
    {!! Form::textarea('chamada_3', null, ['class' => 'form-control ckeditor', 'data-editor' => 'chamada']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'confeccao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
