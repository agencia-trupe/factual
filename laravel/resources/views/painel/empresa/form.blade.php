@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_1', 'Texto 1') !!}
    {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'empresa']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_2', 'Texto 2') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'empresa']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
