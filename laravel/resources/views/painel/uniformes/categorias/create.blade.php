@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Uniformes /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open(['route' => 'painel.uniformes.categorias.store']) !!}

        @include('painel.uniformes.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
