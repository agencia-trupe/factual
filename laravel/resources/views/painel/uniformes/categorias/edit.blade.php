@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Uniformes /</small> Editar Categoria</h2>
    </legend>

    {!! Form::model($categoria, [
        'route' => ['painel.uniformes.categorias.update', $categoria->id],
        'method' => 'patch'])
    !!}

        @include('painel.uniformes.categorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
