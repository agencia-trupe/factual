@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Uniformes /</small> Editar Uniforme</h2>
    </legend>

    {!! Form::model($uniforme, [
        'route'  => ['painel.uniformes.update', $uniforme->id],
        'method' => 'patch',
        'files'  => true])
    !!}

        @include('painel.uniformes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@stop
