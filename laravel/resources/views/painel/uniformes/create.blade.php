@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Uniformes /</small> Adicionar Uniforme</h2>
    </legend>

    {!! Form::open(['route' => 'painel.uniformes.store', 'files' => true]) !!}

        @include('painel.uniformes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@stop
