@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Newsletter
            @if(count($emails))
            <a href="{{ route('painel.newsletter.exportar') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-download-alt" style="margin-right:10px;"></span>Exportar CSV</a>
            @endif
        </h2>
    </legend>

    @if(!count($emails))
    <div class="alert alert-warning" role="alert">Nenhum e-mail cadastrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover ">
        <thead>
            <tr>
                <th>E-mail</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($emails as $email)

            <tr class="tr-row">
                <td>{{ $email->email }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.newsletter.destroy', $email->id],
                        'method' => 'delete'
                    ]) !!}

                    <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {!! $emails->render() !!}
    @endif

@stop