<ul class="nav navbar-nav">
	<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.home.index') }}">Home</a>
	</li>
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.empresa*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.empresa.index') }}">Empresa</a>
    </li>
    <li @if(str_is('painel.uniformes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.uniformes.index') }}">Uniformes</a>
    </li>
    <li @if(str_is('painel.confeccao*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.confeccao.index') }}">Confecção</a>
    </li>
    <li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.clientes.index') }}">Clientes</a>
    </li>
    <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
