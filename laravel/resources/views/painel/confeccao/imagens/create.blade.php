@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Confecção /</small> Adicionar Imagem</h2>
    </legend>

    {!! Form::open(['route' => 'painel.confeccao.imagens.store', 'files' => true]) !!}

        @include('painel.common.flash')

        <div class="form-group">
            {!! Form::label('imagem', 'Imagem') !!}
            {!! Form::file('imagem', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('legenda', 'Legenda (opcional)') !!}
            {!! Form::text('legenda', null, ['class' => 'form-control']) !!}
        </div>

        {!! Form::submit('Inserir', ['class' => 'btn btn-success']) !!}

        <a href="{{ route('painel.confeccao.index') }}" class="btn btn-default btn-voltar">Voltar</a>

    {!! Form::close() !!}

@endsection
