@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Confecção
            <a href="{{ route('painel.confeccao.imagens.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Imagem</a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.confeccao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.confeccao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

    <hr>

    @if(!count($imagens))
    <div class="alert alert-warning" role="alert">Nenhuma imagem encontrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="confeccao_imagens">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Imagem</th>
                <th>Legenda</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($imagens as $imagem)
            <tr class="tr-row" id="{{ $imagem->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><img src="{{ url('assets/img/confeccao/thumbs/'.$imagem->imagem) }}" style="display:block; width: 100%; max-width: 100px;"></td>
                <td>{{ $imagem->legenda }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.confeccao.imagens.destroy', $imagem->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
