(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.envioNewsletter = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-newsletter-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/newsletter',
            data: {
                email: $('#newsletter_email').val()
            },
            success: function(data) {
                $form[0].reset();
                $response.hide().text(data.message).fadeIn('slow');
            },
            error: function(data) {
                var errorMsg = data.responseJSON.nome ? data.responseJSON.nome : data.responseJSON.email;
                $response.hide().text(errorMsg).fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.galeriaFancybox = function() {
        $('.fancybox').fancybox({
            padding: 10,
            nextSpeed: 500,
            prevSpeed: 500,
            helpers: {
                title: { type: 'inside' },
                overlay: { locked: false }
            }
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                $response.fadeOut().text('Preencha todos os campos corretamente.').fadeIn('slow');
            },
            dataType: 'json'
        });
    };

    App.bannersHome = function() {
        $('.banners-cycle').cycle({
            slides: '.slide',
            pager: '.cycle-pager',
            pagerTemplate: '<a href="#">{{slideNum}}</a>',
            autoHeight: 'calc',
            fx: 'scrollHorz'
        });
    };

    App.init = function() {
        this.mobileToggle();
        $('#form-newsletter').on('submit', this.envioNewsletter);
        $('#form-contato').on('submit', this.envioContato);
        this.galeriaFancybox();
        this.bannersHome();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
