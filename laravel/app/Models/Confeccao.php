<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Confeccao extends Model
{
    protected $table = 'confeccao';

    protected $guarded = ['id'];
}
