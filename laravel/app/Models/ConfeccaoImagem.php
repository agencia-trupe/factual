<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfeccaoImagem extends Model
{
    protected $table = 'confeccao_imagens';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
