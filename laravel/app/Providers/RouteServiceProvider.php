<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('categorias', 'App\Models\UniformeCategoria');
        $router->model('uniformes', 'App\Models\Uniforme');
		$router->model('clientes', 'App\Models\Cliente');
        $router->model('confeccao', 'App\Models\Confeccao');
		$router->model('imagens', 'App\Models\ConfeccaoImagem');
		$router->model('empresa', 'App\Models\Empresa');
		$router->model('banners', 'App\Models\Banner');
        $router->model('home', 'App\Models\Home');
		$router->model('newsletter', 'App\Models\Newsletter');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('categoria_slug', function($value) {
            return \App\Models\UniformeCategoria::with('uniformes')->slug($value)->first() ?: \App::abort('404');
        });
        $router->bind('uniforme_slug', function($value) {
            return \App\Models\Uniforme::slug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
