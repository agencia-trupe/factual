<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa', 'EmpresaController@index')->name('empresa');
    Route::get('uniformes-escolares/{categoria_slug?}', 'UniformesController@index')->name('uniformes');
    Route::get('uniformes-escolares/{categoria_slug}/{uniforme_slug}', 'UniformesController@show')->name('uniformes.show');
    Route::get('confeccao', 'ConfeccaoController@index')->name('confeccao');
    Route::get('clientes', 'ClientesController@index')->name('clientes');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');
    Route::post('newsletter', 'NewsletterController@envio');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
        Route::resource('uniformes/categorias', 'UniformesCategoriasController');
        Route::resource('uniformes', 'UniformesController');
		Route::resource('clientes', 'ClientesController');
        Route::resource('confeccao/imagens', 'ConfeccaoImagensController', ['only' => ['create', 'store', 'destroy']]);
		Route::resource('confeccao', 'ConfeccaoController', ['only' => ['index', 'update']]);
		Route::resource('empresa', 'EmpresaController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
        Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
        Route::resource('newsletter', 'NewsletterController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
