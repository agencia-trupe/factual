<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Uniforme;
use App\Models\UniformeCategoria;

class UniformesController extends Controller
{
    public function index(UniformeCategoria $categoria)
    {
        if (! $categoria->exists) {
            $categoria = UniformeCategoria::ordenados()->first() ?: \App::abort('404');
        }

        $uniformes = $categoria->uniformes()->get();

        return view('frontend.uniformes.index', compact('categoria', 'uniformes'));
    }

    public function show(UniformeCategoria $categoria, Uniforme $uniforme)
    {
        return view('frontend.uniformes.show', compact('categoria', 'uniforme'));
    }
}
