<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Confeccao;
use App\Models\ConfeccaoImagem;

class ConfeccaoController extends Controller
{
    public function index()
    {
        $confeccao = Confeccao::first();
        $imagens   = ConfeccaoImagem::ordenados()->get();

        return view('frontend.confeccao', compact('confeccao', 'imagens'));
    }
}
