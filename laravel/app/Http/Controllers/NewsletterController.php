<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\NewsletterRequest;

use App\Models\Newsletter;

class NewsletterController extends Controller
{
    public function envio(NewsletterRequest $request, Newsletter $newsletter)
    {
        $newsletter->create($request->all());

        $response = [
            'status'  => 'success',
            'message' => 'cadastro efetuado com sucesso!'
        ];

        return response()->json($response);
    }
}
