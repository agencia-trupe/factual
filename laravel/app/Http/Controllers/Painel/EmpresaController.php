<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EmpresaRequest;
use App\Http\Controllers\Controller;

use App\Models\Empresa;

class EmpresaController extends Controller
{
    public function index()
    {
        $registro = Empresa::first();

        return view('painel.empresa.edit', compact('registro'));
    }

    public function update(EmpresaRequest $request, Empresa $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.empresa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
