<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\UniformesRequest;
use App\Http\Controllers\Controller;

use App\Models\UniformeCategoria;
use App\Models\Uniforme;
use App\Helpers\CropImage;

class UniformesController extends Controller
{
    private $categorias;

    private $image_config = [
        'capa' => [
            'width'   => 270,
            'height'  => 200,
            'path'    => 'assets/img/uniformes/thumbs/'
        ],
        'imagem' => [
            'width'   => 400,
            'height'  => null,
            'path'    => 'assets/img/uniformes/'
        ],
    ];

    public function __construct()
    {
        $this->categorias = UniformeCategoria::ordenados()->lists('titulo', 'id');
    }

    public function index(Request $request)
    {
        $categorias = $this->categorias;
        $filtro     = $request->query('filtro');

        if (UniformeCategoria::find($filtro)) {
            $uniformes = Uniforme::ordenados()->categoria($filtro)->get();
        } else {
            $uniformes = Uniforme::leftJoin('uniformes_categorias as cat', 'cat.id', '=', 'uniformes_categoria_id')
                ->orderBy('cat.ordem', 'ASC')
                ->orderBy('cat.id', 'DESC')
                ->select('uniformes.*')
                ->ordenados()->get();
        }

        return view('painel.uniformes.index', compact('categorias', 'uniformes', 'filtro'));
    }

    public function create()
    {
        $categorias = $this->categorias;

        return view('painel.uniformes.create', compact('categorias'));
    }

    public function store(UniformesRequest $request)
    {
        try {

            $input = $request->all();
            $input['capa'] = CropImage::make('capa', $this->image_config['capa']);
            $input['imagem'] = CropImage::make('imagem', $this->image_config['imagem']);

            Uniforme::create($input);
            return redirect()->route('painel.uniformes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Uniforme $uniforme)
    {
        $categorias = $this->categorias;

        return view('painel.uniformes.edit', compact('categorias', 'uniforme'));
    }

    public function update(UniformesRequest $request, Uniforme $uniforme)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = CropImage::make('imagem', $this->image_config['imagem']);
            if (isset($input['capa'])) $input['capa'] = CropImage::make('capa', $this->image_config['capa']);

            $uniforme->update($input);
            return redirect()->route('painel.uniformes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Uniforme $uniforme)
    {
        try {

            $uniforme->delete();
            return redirect()->route('painel.uniformes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
