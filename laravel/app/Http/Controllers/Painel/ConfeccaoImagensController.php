<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConfeccaoImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\ConfeccaoImagem;
use App\Helpers\CropImage;

class ConfeccaoImagensController extends Controller
{
    private $image_config = [
        [
            'width'  => 180,
            'height' => 180,
            'path'   => 'assets/img/confeccao/thumbs/'
        ],
        [
            'width'  => 800,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/confeccao/'
        ]
    ];

    public function create()
    {
        return view('painel.confeccao.imagens.create');
    }

    public function store(ConfeccaoImagensRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            ConfeccaoImagem::create($input);
            return redirect()->route('painel.confeccao.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ConfeccaoImagem $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.confeccao.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
