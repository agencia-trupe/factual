<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConfeccaoRequest;
use App\Http\Controllers\Controller;

use App\Models\Confeccao;
use App\Models\ConfeccaoImagem;

class ConfeccaoController extends Controller
{
    public function index()
    {
        $registro = Confeccao::first();
        $imagens = ConfeccaoImagem::ordenados()->get();

        return view('painel.confeccao.edit', compact('registro', 'imagens'));
    }

    public function update(ConfeccaoRequest $request, Confeccao $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.confeccao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
