<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\UniformeCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\UniformeCategoria;

class UniformesCategoriasController extends Controller
{
    public function index()
    {
        $categorias = UniformeCategoria::ordenados()->get();

        return view('painel.uniformes.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.uniformes.categorias.create');
    }

    public function store(UniformeCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            UniformeCategoria::create($input);
            return redirect()->route('painel.uniformes.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(UniformeCategoria $categoria)
    {
        return view('painel.uniformes.categorias.edit', compact('categoria'));
    }

    public function update(UniformeCategoriasRequest $request, UniformeCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.uniformes.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(UniformeCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.uniformes.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
