<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Home;
use App\Models\Banner;

class HomeController extends Controller
{

    public function index()
    {
        $home = Home::first();
        $banners = Banner::ordenados()->get();

        return view('frontend.home', compact('home', 'banners'));
    }

}
