<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Empresa;
use App\Models\UniformeCategoria;

class EmpresaController extends Controller
{
    public function index()
    {
        $empresa = Empresa::first();
        $categorias = UniformeCategoria::has('uniformes')->ordenados()->get();

        return view('frontend.empresa', compact('empresa', 'categorias'));
    }
}
